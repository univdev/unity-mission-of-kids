﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuccessSceneManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static Text text = null;
    void Awake() {
        text = GameObject.Find("Score Text").GetComponent<Text>();
    }

    void Update() {
        int famePerScore = 100;
        int score = Inventory.money + (famePerScore * Status.fame);
        text.text = score.ToString();
    }
}
