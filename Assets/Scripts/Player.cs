﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class VectorParam {
    public Vector3 a;
    public Vector3 b;

    public VectorParam(Vector3 a, Vector3 b) {
        this.a = a;
        this.b = b;
    }
}

class VectorWithInt {
    public Vector3 a;
    public Vector3 b;
    public int index;
    public VectorWithInt(Vector3 a, Vector3 b, int index) {
        this.a = a;
        this.b = b;
        this.index = index;
    }
}

public class Player : MonoBehaviour
{
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
             
    }

    public void Move(int index) {
        GameObject controller = GameObject.FindGameObjectWithTag("GameController");
        GameObject[] places = controller.GetComponent<IngameSceneManager>().GetPlaces();
        GameObject place = places[index];
        Vector3 destination = place.transform.position;
        destination.z = -0.1f;
        int currentIndex = controller.GetComponent<IngameSceneManager>().GetPlayerPosition();

        // for (int i = currentIndex + 1; i <= index; i += 1) {
        //     Vector3 currentPosition = transform.position;
        //     Vector3 destination = place.transform.position;
        //     int j = 1;
        //     StartCoroutine("MoveDelay", new VectorParam(currentPosition, destination));
        // }
        rb.MovePosition(destination);
    }

    // IEnumerator MoveDelay(VectorParam v) {
    //     Vector3 currentPosition = v.a;
    //     Vector3 destination = v.b;
    //     for (int j = 0; j < 100; j += 1) {
    //         StartCoroutine("MoveAnimation", new VectorWithInt(currentPosition, destination, j));
    //     }
    //     yield return new WaitForSeconds(2);
    // }

    // IEnumerator MoveAnimation(VectorWithInt v) {
    //     Vector3 destination = Vector3.Lerp(v.a, v.b, 0.01f * v.index);
    //     rb.MovePosition(destination);
    //     yield return new WaitForSeconds(0.01f);
    // }
}
