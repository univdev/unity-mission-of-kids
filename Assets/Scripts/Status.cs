﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Status : MonoBehaviour
{
    // Start is called before the first frame update
    public static int fame = 10;
    private Text text;

    void Awake() {
        Transform numberCover = GameObject.FindGameObjectWithTag("Medal GUI").transform.Find("Number Cover");
        text = numberCover.Find("Text").GetComponent<Text>();
    }
    void Update() {
        text.text = fame.ToString();
    }
}
