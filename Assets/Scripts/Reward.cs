﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Rewards", menuName = "New Asset/New Reward")]
public class Reward : ScriptableObject
{
    public int fame = 0; // 명성
    public int money = 0; // 돈
    public string placeType = "Add"; // Add or Absolute
    public int place = -1; // 이동할 장소 번호
    public int time = 0; // 시간
}
