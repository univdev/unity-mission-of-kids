﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorInputManager : MonoBehaviour
{
    private static string color = "";

    void Update() {
        AnswerManager.SetColorCard(color);
    }
    public static void SetColor(string c) {
        color = c;
    }
    public static string GetColor() {
        return color;
    }
    public static void Clear() {
        color = "";
    }
}
