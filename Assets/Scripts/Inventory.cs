﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public static int money = 1000;

    private Text text;

    void Awake() {
        Transform numberCover = GameObject.FindGameObjectWithTag("Money GUI").transform.Find("Number Cover");
        text = numberCover.Find("Text").GetComponent<Text>();
    }

    void Update() {
        text.text = money.ToString();
    }
}
