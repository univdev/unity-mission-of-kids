﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameSoundEffect : MonoBehaviour
{
    // Start is called before the first frame update
    public static GameObject soundEffectObject;
    public static AudioSource soundEffect;
    void Awake() {
        soundEffectObject = GameObject.Find("Sound Effect");
        soundEffect = soundEffectObject.GetComponent<AudioSource>();
    }

    public static void Change(AudioClip clip) {
        soundEffect.clip = clip;
    }
    public static void Play() {
        soundEffect.Play();
    }
    public static void Stop() {
        soundEffect.Stop();
    }
    public static void Pause() {
        soundEffect.Pause();
    }
}
