﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Uduino;

public class IngameSceneManager : MonoBehaviour
{
    /*
     * 아두이노 연결 상태를 통해 에러메시지를 표시함.
     * 
     * GameObject messageBox
     * - 아두이노 연결을 요구하는 메시지박스
     * bool isConnectedArduino : false
     * - 해당 구분자가 true면 경고문 비활성화, false면 활성화
     */
    public GameObject messageBox;
    public static bool isConnectedArduino = false;

    /**
     * 게임 구현부
     * 
     * float timer : 600f
     * - 게임 시간은 10분이며, 이 타이머가 종료되면 게임 오버
     * int playerPosition : 0
     * - 플레이어가 존재하는 칸의 인덱스
     * GameObject[] places
     * - 게임 내 존재하는 칸의 수
     * int situation : 0
     * - 현재 플레이어가 취해야 할 상황
     * - 0: 아무런 동작도 취할 수 없는 상황
     * - 1: 주사위를 던질 수 있는 상황
     * - 2: 퀴즈를 맞춰야하는 상황
     * 
     */

    private GameObject player;
    private Player playerScript;
    private float timer = 400f;
    private Text timerUI = null;
    private int playerPosition = 0;
    public GameObject[] places;
    public AudioClip successSoundEffect = null;
    public AudioClip denySoundEffect = null;
    public AudioClip wrongSoundEffect = null;
    private int situation = (int)PlayerSituations.NEVER;

    private IEnumerator timerCoroutine;
    private AudioClip[] numberAudioClip;
    private bool isReadyDice = false;

    void Start()
    {
        Init();
    }

    void Init() {
        Inventory.money = 1000;
        Status.fame = 10;

        // places = GameObject.FindGameObjectsWithTag("Place");
        player = GameObject.FindGameObjectWithTag("Player");
        playerScript = player.GetComponent<Player>();
        timerCoroutine = ProgressTimer();
        timerUI = GameObject.FindGameObjectWithTag("Timer GUI").transform.Find("Number Cover").Find("Text").GetComponent<Text>();

        // UduinoManager.Instance.pinMode(12, PinMode.Input_pullup);
        UduinoManager.Instance.OnDataReceived += dataReceiver;

        /* 숫자 효과음 리소스 호출 */
        numberAudioClip = new AudioClip[6];
        numberAudioClip[1] = Resources.Load<AudioClip>("SoundEffects/숫자_2");
        numberAudioClip[2] = Resources.Load<AudioClip>("SoundEffects/숫자_3");
        numberAudioClip[3] = Resources.Load<AudioClip>("SoundEffects/숫자_4");
        numberAudioClip[4] = Resources.Load<AudioClip>("SoundEffects/숫자_5");
        numberAudioClip[5] = Resources.Load<AudioClip>("SoundEffects/숫자_6");

        SetTimerUI();
    }

    void Update()
    {
        int buttonState = UduinoManager.Instance.digitalRead(12); // 버튼 눌림 상태 (Pullup이라서 1이 부정, 0이 긍정)

        messageBox.SetActive(!isConnectedArduino); // 아두이노 연결을 요구하는 메시지 박스의 출력 여부를 결정함.
    }

    public void PlayerMove(int step) { // ? 플레이어의 움직임과 관련된 모든 현상을 다루는 메소드
        SetPlayerPosition(step);
        playerScript.Move(GetPlayerPosition());
        RollDiceText.Show(false);

        if (GetPlayerPosition() >= places.Length - 1) {
            if (Inventory.money >= 1000)
                Goal();
            else
                GameOver();
            return;
        }

        Mission mission = GetCurrentMission();

        // 미션이 없다면 주사위를 다시 굴리도록 진행함.
        if (!mission) {
            SetMode(PlayerSituations.DICE);
            return;
        }

        MissionManager.SetMission(mission); // 미션을 미션 매니저에 할당

        // 미션 매니저에 할당 된 미션에 따른 역할 할당
        switch (MissionManager.GetMissionType()) {
            case "Select": // 단순 선택
                SetMode(PlayerSituations.SELECT);
                break;
            
            case "Quiz": // 퀴즈
                SetMode(PlayerSituations.QUIZ);
                break;
            
            case "Story": // 스토리
                SetMode(PlayerSituations.STORY);
                break;
            
            default: // 그 외
                SetMode(PlayerSituations.DICE);
                break;
        }
        
        MissionManager.Set();
        MissionManager.Show(true);
    }

    public void SetMode(int situation) {
        SetSituation(situation); // 플레이어 모드 변경
        switch (situation) {
            case PlayerSituations.DICE: // 플레이어가 현재 주사위를 던져야 하는 상황일 때
                MissionManager.Show(false);
                RollDiceText.Show(true);
                break;
            
            case PlayerSituations.NEVER:
                MissionManager.Show(false);
                break;
        }
    }

    void dataReceiver(string data, UduinoDevice board) {
        SelectRoot(data);
    }

    void SelectRoot(string data) {
        Debug.Log("Serial - " + data);

        if (data.IndexOf(":") < 0) return;

        string[] options = data.Split(':');
        string optionName = options[0];
        string optionContent = options[1];

        bool firstButton = data.Equals("Button:First");
        bool firstButtonDown = data.Equals("Button:FirstDown");
        bool firstButtonUp = data.Equals("Button:FirstUp");

        bool secondButton = data.Equals("Button:Second");
        bool secondButtonDown = data.Equals("Button:SecondDown");
        bool secondButtonUp = data.Equals("Button:SecondUp");

        bool normalTouch = data.Equals("Touch:Default");

        switch (situation) {
            // 플레이어가 주사위를 던질 차례일 때
            case PlayerSituations.DICE:
                SetMode(PlayerSituations.DICE);
                if (firstButtonDown && !isReadyDice) { // 1번 버튼이 눌렸을 때
                    GaugeManager.SetActive(true); // 주사위 던지는 파워를 보여줌
                    isReadyDice = true;
                }

                if (firstButtonUp && isReadyDice) {
                    isReadyDice = false;
                    GaugeManager.SetActive(false); // 주사위 던지는 파워 숨김
                    int num = RollDice(); // 1 ~ 6 숫자 구함
                    AudioClip clip = (AudioClip)numberAudioClip.GetValue(num - 1);
                    DiceNumber.currentNumber = num;
                    RollDiceText.Show(false);

                    if (clip != null) {
                        IngameSoundEffect.Change(clip);
                        IngameSoundEffect.Play();
                    }

                    DiceNumberText.Set(num); // 주사위 숫자로 텍스트 변경
                    DiceNumberText.Show(true); // 주사위 숫자 보이기
                    SetSituation(PlayerSituations.NEVER); // 관전모드로 진입
                }
                break;
            
            // 플레이어가 선택할 차례일 때
            case PlayerSituations.SELECT:
                if (firstButton) { // 미션 수락
                    PlaySuccessSoundEffect(); // 성공 사운드
                    MissionManager.GiveRewards(); // 보상 지급
                    IngameNoticeTextManager.SetActiveAcceptText(true);
                    SetMode(PlayerSituations.NEVER); // 결정 후에도 연속으로 눌러서 보상을 여러번 취하는 것을 방지
                } else if (secondButton) { // 미션 스킵
                    PlayDenySoundEffect(); // 거절 사운드
                    MissionManager.GivePenalty(); // 패널티 지급
                    IngameNoticeTextManager.SetActiveDenyText(true);
                    SetMode(PlayerSituations.NEVER); // 결정 후에도 연속으로 눌러서 보상을 여러번 취하는 것을 방지
                }
                break;

            case PlayerSituations.QUIZ:
                if (optionName.Equals("Tag UID")) {
                    InputCard(optionContent);
                    return;
                }

                if (normalTouch) {
                    AnswerManager.Clear();
                    return;
                }

                if (firstButton) { // 퀴즈 정답 제출
                    /**
                     * TODO: 해결할 문제가 수학 문제인지, 색깔 문제인지 구분하여 해결 함.
                     */
                    string answer = MissionManager.GetAnswer();
                    string numberAnswer = NumberInputManager.GetString();
                    string colorAnswer = ColorInputManager.GetColor();

                    if (answer.Equals(numberAnswer) || answer.Equals(colorAnswer)) {
                        PlaySuccessSoundEffect();
                        MissionManager.GiveRewards();
                        IngameNoticeTextManager.SetActiveSuccessText(true);
                        SetMode(PlayerSituations.NEVER);
                        AnswerManager.Clear();
                    } else {
                        PlayDenySoundEffect();
                        NoticeText.SetEvent(false); // 오답이면 여러번 도전할 수 있도록 애니메이션 이벤트를 차단해 둠.
                        IngameNoticeTextManager.SetActiveFailText(true);
                        AnswerManager.Clear();
                    }

                } else if (secondButton) { // 퀴즈 스킵
                    PlayDenySoundEffect(); // 거절 사운드
                    MissionManager.GivePenalty(); // 패널티 지급
                    IngameNoticeTextManager.SetActiveDenyText(true);
                    SetMode(PlayerSituations.NEVER); // 결정 후에도 연속으로 눌러서 보상을 여러번 취하는 것을 방지
                }
                break;

            case PlayerSituations.STORY:
                if (firstButton) {
                    MissionManager.GiveRewards();
                    SetMode(PlayerSituations.DICE);
                }
                break;
        }
    }

    private void InputCard(string uid) {
        int number = NumberCardManager.IndexOf(uid);
        string color = ColorCardManager.GetColor(uid);
        Debug.Log(color);

        if (number >= 0) {
            NumberInputManager.Push(number);
            return;
        }

        if (!color.Equals("")) {
            ColorInputManager.SetColor(color);
            return;
        }
    }

    public void SetConnectArduino(bool flag) {
        isConnectedArduino = flag;
    }

    public GameObject[] GetPlaces() {
        return places;
    }

    public void SetPlayerPosition(int position) {
        if (places.Length <= position) position = places.Length - 1;
        playerPosition = position;
    }

    public int GetPlayerPosition() {
        return playerPosition;
    }

    public void SetSituation(int situation) {
        this.situation = situation;
    }
    public void ToggleTimer(bool flag) {
        if (flag)
            StartCoroutine(timerCoroutine);
        else
            StopCoroutine(timerCoroutine);
    }

    IEnumerator ProgressTimer() {
        while (timer > 0) {
            yield return new WaitForSeconds(1);
            timer -= 1;
            SetTimerUI();
        }

        if (timer <= 0) GameOver();
    }

    public void SetTimer(float time) {
        timer = time;
    }
    public float GetTime() {
        return timer;
    }

    int RollDice() {
        return Random.Range(1, 7);
    }

    void GameOver() {
        SceneManager.LoadScene("FailScene", LoadSceneMode.Single);
    }

    void Goal() {
        SceneManager.LoadScene("SuccessScene", LoadSceneMode.Single);
    }

    Mission GetCurrentMission() {
        if (places[GetPlayerPosition()].GetComponent<Place>() == null) {
            return null;
        }
        return places[GetPlayerPosition()].GetComponent<Place>().mission;
    }

    void SetTimerUI() {
        timerUI.text = timer.ToString();
    }

    void PlaySuccessSoundEffect() {
        if (successSoundEffect == null) return;
        IngameSoundEffect.Change(successSoundEffect);
        IngameSoundEffect.Play();
    }
    void PlayDenySoundEffect() {
        if (denySoundEffect == null) return;
        IngameSoundEffect.Change(denySoundEffect);
        IngameSoundEffect.Play();
    }
    void PlayWrongSoundEffect() {
        if (wrongSoundEffect == null) return;
        IngameSoundEffect.Change(wrongSoundEffect);
        IngameSoundEffect.Play();
    }
}
