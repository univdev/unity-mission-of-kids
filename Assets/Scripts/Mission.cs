﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Mission", menuName = "New Asset/New Mission")]
public class Mission : ScriptableObject
{
    // Start is called before the first frame update
    public string type;
    public string title;
    [TextArea]
    public string description;
    public Reward rewards;
    public Reward penalty;
    public string anwser;
}
