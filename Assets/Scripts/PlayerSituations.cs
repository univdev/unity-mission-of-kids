﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSituations : MonoBehaviour
{
    public const int NEVER = 0; // 보고만 있어야하는 상황
    public const int DICE = 1; // 주사위를 던지는 상황
    public const int QUIZ = 2; // 퀴즈를 맞추는 상황
    public const int SELECT = 3; // 단순 긍정 / 부정 선택지
    public const int STORY = 4; // 스토리
}
