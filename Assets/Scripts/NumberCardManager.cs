﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberCardManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static string[] numberCardsUID = {
        "k341gCA1OB21HAB", "1~E1?4C1?FAR1B", "x41?901}E0T1D", "1nD1L15?6ER1B", "1nD1L15?6BR1B",
        "1?F1881?FAR1B", "1?F11SB6L15Q1A", "1~E1CC]26R1B", "1nD1991?FBR1B", "1?F11eC8?70Q1A"
    };
    
    public static int IndexOf(string uid) {
        int index = -1;
        for (int i = 0; i < numberCardsUID.Length; i += 1) {
            if (numberCardsUID[i].Equals(uid)) {
                index = i;
                break;
            }
        }
        return index;
    }
}
