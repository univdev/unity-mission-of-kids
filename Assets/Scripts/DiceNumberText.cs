﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiceNumberText : MonoBehaviour
{
    private static GameObject text;

    void Awake() {
        text = GameObject.Find("Dice Number");
        Show(false);
    }
    public static void Show(bool flag) {
        text.SetActive(flag);
    }
    public static void Set(int number) {
        text.GetComponent<Text>().text = number.ToString();
    }
}
