﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RollDiceText : MonoBehaviour
{
    // Start is called before the first frame update
    private static GameObject text;

    void Awake() {
        text = GameObject.Find("Roll the dice Text");
        Show(false);
    }

    public static void Show(bool flag) {
        text.SetActive(flag);
    }
}
