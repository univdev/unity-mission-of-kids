﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoticeText : MonoBehaviour
{
    // Start is called before the first frame update
    private IngameSceneManager sceneManager;
    private static bool isEvent = true;

    private void Awake() {
        GameObject sceneManagerObject = GameObject.FindGameObjectWithTag("GameController");
        sceneManager = sceneManagerObject.GetComponent<IngameSceneManager>();
    }
    void OnAnimationEnded() {
        gameObject.SetActive(false);
        if (!isEvent) {
            isEvent = true; // 이벤트 실행 차단은 1회성으로 제한
            return;
        };
        // TODO: 주사위 굴릴 수 있는 상태로 전환
        sceneManager.SetMode(PlayerSituations.DICE);
        IngameNoticeTextManager.Clear();
    }

    public static void SetEvent(bool flag) {
        isEvent = flag;
    }
}
